VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   Caption         =   "画像分割"
   ClientHeight    =   6390
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   8760
   LinkTopic       =   "Form1"
   ScaleHeight     =   426
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   584
   StartUpPosition =   2  '画面の中央
   Begin VB.PictureBox Picture2 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   1350
      Left            =   4350
      ScaleHeight     =   90
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   138
      TabIndex        =   1
      Top             =   1230
      Visible         =   0   'False
      Width           =   2070
   End
   Begin MSComDlg.CommonDialog dlgOpen 
      Left            =   4230
      Top             =   465
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Filter          =   "全て|*.bmp;*.jpg;*.jpeg;*.gif|BITMAP|*.bmp|JPEG|*.jpg;*.jpeg|GIF|*.gif"
      Flags           =   4100
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   1320
      Left            =   945
      ScaleHeight     =   88
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   161
      TabIndex        =   0
      Top             =   1305
      Width           =   2415
   End
   Begin VB.Menu mnuOpen 
      Caption         =   "画像を開く"
   End
   Begin VB.Menu mnuYoko 
      Caption         =   "横の分割数(2)"
   End
   Begin VB.Menu mnuTate 
      Caption         =   "縦の分割数(2)"
   End
   Begin VB.Menu mnuSize 
      Caption         =   "画像のサイズ(0x0)"
   End
   Begin VB.Menu mnuDvSize 
      Caption         =   "分割のサイズ(0x0)"
   End
   Begin VB.Menu mnuRun 
      Caption         =   "分割を実行"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim dw As Integer, dh As Integer
Dim w%, h%, ww%, hh%

Dim mx!, my!

Public Event Keika(tm As Single)

Private Sub Form_Load()
  
  Picture1.Top = 0
  Picture1.Left = 0
  
  dw = 2
  dh = 2
  
End Sub

Private Sub mnuDvSize_Click()
  Call DivideLine
End Sub

Private Sub mnuOpen_Click()
  
  On Error GoTo mnuOpen_ERROR

  dlgOpen.ShowOpen
  
  Picture1.Picture = Nothing
  
  Picture1.Picture = LoadPicture(dlgOpen.FileName)
  Me.Caption = "画像分割 " & dlgOpen.FileTitle
  Picture1.Refresh
  
  w = Picture1.Width
  h = Picture1.Height
  
  ww = w / dw
  hh = h / dh
  
  Picture2.Width = ww
  Picture2.Height = hh
  
  mnuSize.Caption = "画像のサイズ(" & w & "x" & h & ")"
  
  mnuDvSize.Caption = "分割のサイズ(" & ww & "x" & hh & ")"
  
  Call DivideLine
  
  Exit Sub

mnuOpen_ERROR:

  'Cancel
  
End Sub

Private Sub mnuRun_Click()
  Dim i%, j%, n%
  Dim X!, Y!
  Dim df$, ddf$, fn$

  If w = 0 Or h = 0 Then
    Exit Sub
  End If

  Picture1.Refresh
  
  Form2.Show vbModeless, Me
  
  Set Form2.Form1 = Me
  
  Me.Enabled = False
  
  df = dlgOpen.FileTitle
  
  ddf = StrReverse(df)
  n = InStr(1, ddf, ".")
  If n > 0 Then
    df = Left(df, Len(ddf) - n)
  End If
  
  If Len(df) > 5 Then
    df = Left(df, 5)
  End If
  
  ddf = Right("00000" & Int(Timer()), 5)
  
  RaiseEvent Keika(0)
  n = 0
  For i = 0 To dh - 1
    Y = i * hh
    For j = 0 To dw - 1
      X = j * ww
      Picture2.Cls
      Picture2.PaintPicture Picture1.Image, 0, 0, , , X, Y, ww, hh
      
      n = n + 1
      fn = df & "_" & ddf & Right("000" & n, 3) & ".bmp"
      
      SavePicture Picture2.Image, fn
      
      RaiseEvent Keika(100! * CSng(n) / CSng(dh * dw))
      
    Next j
  Next i
  
  Set Form2.Form1 = Nothing
  Unload Form2
  
  Me.Enabled = True
  
  MsgBox "分割に成功しました。", , "画像分割"

End Sub

Private Sub mnuSize_Click()
  Picture1.Refresh

End Sub

Private Sub mnuTate_Click()
  Dim a As Integer
  
  a = -9756
  
  a = Val(InputBox("縦の分割数？ (1 - 20)", "画像分割", dh))
  
  If a = -9756 Then
    Exit Sub
  End If
  
  If a < 1 Then
    a = 1
  ElseIf a > 20 Then
    a = 20
  End If
  
  dh = a
  hh = h / dh
  Picture2.Height = hh
  
  mnuTate.Caption = "縦の分割数(" & dh & ")"
  mnuDvSize.Caption = "分割のサイズ(" & ww & "x" & hh & ")"

  Call DivideLine

End Sub

Private Sub mnuYoko_Click()
  Dim a As Integer
  
  a = -9756
  
  a = Val(InputBox("横の分割数？ (1 - 20)", "画像分割", dw))
  
  If a = -9756 Then
    Exit Sub
  End If
  
  If a < 1 Then
    a = 1
  ElseIf a > 20 Then
    a = 20
  End If
  
  dw = a
  ww = w / dw
  Picture2.Width = ww
  
  mnuYoko.Caption = "横の分割数(" & dw & ")"
  mnuDvSize.Caption = "分割のサイズ(" & ww & "x" & hh & ")"
  
  Call DivideLine
  
End Sub

Private Sub Picture1_DblClick()
  Picture1.Top = 0
  Picture1.Left = 0
End Sub

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    mx = X
    my = Y
  End If
End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    With Picture1
      .Left = .Left + (X - mx)
      .Top = .Top + (Y - my)
    End With
  End If

End Sub

Sub DivideLine()
  Dim i As Integer

  Picture1.Refresh

  Picture1.AutoRedraw = False
  
  Picture1.DrawMode = vbInvert
  
  For i = 0 To dh
    Picture1.Line (0, i * hh)-(w, i * hh)
  Next i
  
  For i = 0 To dw
    Picture1.Line (i * ww, 0)-(i * ww, h)
  Next i
  
  Picture1.DrawMode = vbCopyPen
  
  Picture1.AutoRedraw = True

End Sub
